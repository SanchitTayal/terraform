variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}

variable "vpc_instance_tenancy" {
  default = "dedicated"
}

variable "subnet_cidr_block" {
  default = "10.0.1.0/24"
}

output "subnet_id" {
  value = aws_subnet.subnet_modules.id
}