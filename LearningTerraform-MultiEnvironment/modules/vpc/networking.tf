resource "aws_vpc" "vpc_modules" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = var.vpc_instance_tenancy

  tags = {
    Name = "vpc_modules"
  }
}

resource "aws_subnet" "subnet_modules" {
  vpc_id     = aws_vpc.vpc_modules.id
  cidr_block = var.subnet_cidr_block

  tags = {
    Name = "subnet_modules"
  }
}