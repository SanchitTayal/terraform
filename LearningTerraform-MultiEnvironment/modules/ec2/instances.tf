resource "aws_instance" "modules_ec2" {
  ami           = var.ami_id
  count         = var.ec2_count
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
  tags = {
    Name = "module_ec2"
  }
}