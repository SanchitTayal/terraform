provider "aws" {
  region = "us-east-2"
}
module "dev_vpc" {
  source                = "../modules/vpc"
  vpc_cidr_block        = "192.168.0.0/16"
  vpc_instance_tenancy  = "default"
  subnet_cidr_block     = "192.168.1.0/24"
}

module "dev_ec2" {
  source                = "../modules/ec2"
  ec2_count             = 2
  ami_id                = "ami-07c1207a9d40bc3bd"
  instance_type         = "t2.micro"
  subnet_id             = "${module.dev_vpc.subnet_id}"
}
