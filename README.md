WHY SHOULD YOU USE TERRAFORM?

a. Terraform lets you define infrastructure in config/code and will enable you to rebuild/change and track changes 
   to infrastructure with ease. Terraform provides a high-level description of infrastructure.

b. Almost all other tools have a severe impedance mismatch from trying to wrangle an API designed for configuring management 
   to control an infrastructure environment. Instead, Terraform matches correctly with what you want to do the API aligns with 
   the way you think about infrastructure.
   
c. Terraform is the only sophisticated tool that is completely platform agnostic as well as supports other services while there 
   are a few alternatives but they are focused on a single cloud provider
   
And hence I started my Terraform journey in 2019. Here are a few of my projects which might not be 5000 lines code but covers
most of the important features of Terraform. Some of the are as follows :

1. MutiEnvironment use of modules.
2. Encapsulating a resource with main, variables and output files.
3. Creating Modules that are reusable for most of the code.
