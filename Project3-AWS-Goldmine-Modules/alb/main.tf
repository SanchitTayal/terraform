provider "aws" {
  region = "us-east-2"
}

resource "aws_lb_target_group" "sanchit-terraform-target-group" {
  health_check {
    interval            = 10
    path                = "/"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }

  name        = "sanchit-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"
}

resource "aws_lb_target_group_attachment" "sanchit-terraform-target-group-attachment1" {
  target_group_arn = "${aws_lb_target_group.sanchit-terraform-target-group.arn}"
  target_id        = "${var.instance1_id}"
  port             = 80
}

resource "aws_lb_target_group_attachment" "sanchit-terraform-target-group-attachment2" {
  target_group_arn = "${aws_lb_target_group.sanchit-terraform-target-group.arn}"
  target_id        = "${var.instance2_id}"
  port             = 80
}

resource "aws_security_group" "sanchit-terraform-alb-sg" {
  name   = "my-alb-sg"
  vpc_id = "${var.vpc_id}"
}

resource "aws_lb" "sanchit-terraform-alb" {
  name     = "sanchit-alb"
  internal = false

  security_groups = [
    "${aws_security_group.sanchit-terraform-alb-sg.id}",
  ]

  subnets = [
    "${var.subnet1}",
    "${var.subnet2}",
  ]

  tags = {
    Name = "sanchit-alb"
  }

  ip_address_type    = "ipv4"
  load_balancer_type = "application"
}

resource "aws_lb_listener" "sanchit-terraform-alb-listner" {
  load_balancer_arn = "${aws_lb.sanchit-terraform-alb.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.sanchit-terraform-target-group.arn}"
  }
}

resource "aws_security_group_rule" "inbound_ssh" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.sanchit-terraform-alb-sg.id}"
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_http" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.sanchit-terraform-alb-sg.id}"
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.sanchit-terraform-alb-sg.id}"
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}