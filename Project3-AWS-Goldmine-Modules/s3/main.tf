provider "aws" {
  region = "us-east-2"
}

resource "random_id" "sanchit-random-id" {
  byte_length = 2
}

resource "aws_s3_bucket" "sanchit-terraform-bucket" {
  bucket = "${var.s3_bucket_name}-${random_id.sanchit-random-id.dec}"
  acl    = "private"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true

    transition {
      storage_class = "STANDARD_IA"
      days          = 30
    }
  }

  tags = {
    Name = "sanchit-terraform-s3-bucket"
  }
}