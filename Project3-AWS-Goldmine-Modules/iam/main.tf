provider "aws" {
  region = "us-east-2"
}

resource "aws_iam_user" "sanchit-user" {
  name  = "${element(var.username,count.index)}"
  count = "${length(var.username)}"
}

resource "aws_iam_role_policy" "sanchit-iam-policy" {
  name = "sanchit-iam-policy"
  role = "${aws_iam_role.sanchit-iam-role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData",
                "ec2:DescribeTags",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
                "logs:DescribeLogGroups",
                "logs:CreateLogStream",
                "logs:CreateLogGroup"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role" "sanchit-iam-role" {
  name = "sanchit-iam-role"

  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
{
"Action": "sts:AssumeRole",
"Principal": {
 "Service": "ec2.amazonaws.com"
},
"Effect": "Allow"
}
]
}
EOF

  tags = {
    tag-key = "sanchit-iam-role"
  }
}

resource "aws_iam_instance_profile" "sanchit-iam-instance-profile" {
  name = "sanchit-iam-instance-profile"
  role = "${aws_iam_role.sanchit-iam-role.name}"
}