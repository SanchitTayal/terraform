variable "my_public_key" {
  #default "/Users/sanchittayal/public-key.txt.pub"
}

variable "instance_type" {
  #default "t2.micro"
}

variable "security_group" {
}

variable "subnets" {
  type = "list"
}