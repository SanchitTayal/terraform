provider "aws" {
  region = "us-east-2"
}

data "aws_availability_zones" "available" {}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] 
}

resource "aws_key_pair" "sanchit-key" {
  key_name   = "sanchit-terraform-key-new"
  public_key = "${file(var.my_public_key)}"
}

data "template_file" "init" {
  template = "${file("${path.module}/userdata.tpl")}"
}

resource "aws_instance" "sanchit-instance" {
  count                  = 2
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.instance_type}"
  key_name               = "${aws_key_pair.sanchit-key.id}"
  vpc_security_group_ids = ["${var.security_group}"]
  subnet_id              = "${element(var.subnets, count.index)}"
  user_data              = "${data.template_file.init.rendered}"

  tags = {
    Name = "sanchit-instance-${count.index + 1}"
  }
}

resource "aws_ebs_volume" "sanchit-ebs" {
  count             = 2
  availability_zone = "${data.aws_availability_zones.available.names[count.index]}"
  size              = 1
  type              = "gp2"
}

resource "aws_volume_attachment" "sanchit-attach" {
  count        = 2
  device_name  = "/dev/xvdh"
  instance_id  = "${aws_instance.sanchit-instance.*.id[count.index]}"
  volume_id    = "${aws_ebs_volume.sanchit-ebs.*.id[count.index]}"
  force_detach = true
}