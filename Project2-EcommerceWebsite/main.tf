provider "aws" {
  region = "us-east-2"
}

variable "availability_zones" {
  description = "AZs in this region to use"
  default     = ["us-east-2a", "us-east-2b"]
  type        = "list"
}

resource "aws_vpc" "main" {
  cidr_block       = "172.31.0.0/16"
  instance_tenancy = "default"
  tags = {
    Name = "main"
  }
}

variable "subnet_cidrs_public" {
  description = "Subnet CIDRs for public subnets (length must match configured availability_zones)"
  default     = ["172.31.0.0/20", "172.31.16.0/20"]
  type        = "list"
}

resource "aws_subnet" "public" {
  count                   = "${length(var.subnet_cidrs_public)}"
  map_public_ip_on_launch = true
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "${var.subnet_cidrs_public[count.index]}"
  availability_zone       = "${var.availability_zones[count.index]}"
}

variable "subnet_cidrs_private" {
  description = "Subnet CIDRs for public subnets (length must match configured availability_zones)"
  default     = ["172.31.32.0/20", "172.31.48.0/20"]
  type        = "list"
}

resource "aws_subnet" "private" {
  count             = "${length(var.subnet_cidrs_public)}"
  vpc_id            = "${aws_vpc.main.id}"
  cidr_block        = "${var.subnet_cidrs_private[count.index]}"
  availability_zone = "${var.availability_zones[count.index]}"
}

#Declare Internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
  tags = {
    Name = "public"
  }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
}

resource "aws_route_table_association" "public" {
  count          = "${length(var.subnet_cidrs_public)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

#Creating EC2 instances
# Create a new instance of the latest Ubuntu 14.04 on an
# t2.micro node with an AWS Tag naming it "HelloWorld"

resource "aws_instance" "web" {
  count         = 2
  ami           = "ami-03ffa9b61e8d2cfda"
  instance_type = "t2.micro"
  subnet_id     = element(aws_subnet.public.*.id, 0)
  #subnet_id    = element(var.subnet_cidrs_public, 0)
  tags = {
    Name = "EC2 instance in AZ1"
  }
}

resource "aws_instance" "web2" {
  count         = 2
  ami           = "ami-03ffa9b61e8d2cfda"
  instance_type = "t2.micro"
  subnet_id     = element(aws_subnet.public.*.id, 1)
  tags = {
    Name = "EC2 instance in AZ2"
  }
}

resource "aws_db_instance" "master_rds" {
  allocated_storage = 20
  storage_type      = "gp2"
  engine            = "mysql"
  engine_version    = "5.7"
  instance_class    = "db.t2.micro"
  name              = "mydb"
  username          = "master_rds"
  password          = "master_rds_password"
  apply_immediately = true
}

resource "aws_db_subnet_group" "master_rds" {
  name       = "master_rds"
  subnet_ids = [element(aws_subnet.private.*.id, 0), element(aws_subnet.private.*.id, 1)]
  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_db_instance" "standby_rds" {
  allocated_storage = 20
  storage_type      = "gp2"
  engine            = "mysql"
  engine_version    = "5.7"
  instance_class    = "db.t2.micro"
  name              = "mydb"
  username          = "master_rds"
  password          = "master_rds_password"
  apply_immediately = true
}

resource "aws_db_subnet_group" "standby_rds" {
  name       = "standby_rds"
  subnet_ids = [element(aws_subnet.private.*.id, 0), element(aws_subnet.private.*.id, 1)]
  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_elb" "elb-public-subnet-az1" {
  name = "elb-public-subnet-az1"
  #availability_zones = ["us-east-2a"]

  listener {
    instance_port     = 8983
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8983/"
    interval            = 30
  }

  instances                   = [element(aws_instance.web.*.id, 0), element(aws_instance.web.*.id, 1)]
  subnets                     = [element(aws_subnet.public.*.id, 0)]
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "public_subnet_instances_in_az1"
  }
}

resource "aws_elb" "elb-public-subnet-az2" {
  name = "elb-public-subnet-az2"
  #availability_zones = ["us-east-2a"]

  listener {
    instance_port     = 8983
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8983/"
    interval            = 30
  }

  instances                   = [element(aws_instance.web2.*.id, 0), element(aws_instance.web2.*.id, 1)]
  subnets                     = [element(aws_subnet.public.*.id, 1)]
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "public_subnet_instances_in_az2"
  }
}

resource "aws_route53_record" "elb-public-az1" {
  zone_id = "Z3JEOOTS4X31YO"
  name    = "testingelb"
  type    = "CNAME"
  ttl     = "5"

  weighted_routing_policy {
    weight = 50
  }

  set_identifier = "elb-public-az1"
  records        = ["aws_elb.elb-public-subnet-az1.dns_name"]
}

resource "aws_route53_record" "elb-public-az2" {
  zone_id = "Z3JEOOTS4X31YO"
  name    = "testingelb"
  type    = "CNAME"
  ttl     = "5"

  weighted_routing_policy {
    weight = 50
  }

  set_identifier = "elb-public-az2"
  records        = ["${aws_elb.elb-public-subnet-az2.dns_name}"]
}